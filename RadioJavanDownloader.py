import urllib.request
import requests
import json
import argparse
from bs4 import BeautifulSoup

parser = argparse.ArgumentParser()
parser.add_argument('--url', required=True, help='play list url (ex : https://www.radiojavan.com/playlists/playlist/mp3'
                                                 '/233dd24ead2a)')
parser.add_argument('--out', default="out.txt", help='output file')

args = parser.parse_args()
playlist_url = args.url
filename = args.out

host = "https://www.radiojavan.com"
print(playlist_url)
print("results will be save at : " + filename)
file = open(filename, "w+")

req = urllib.request.Request(
    playlist_url,
    data=None,
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/35.0.1916.47 Safari/537.36 '
    }
)

response = urllib.request.urlopen(req).read().decode('utf-8')

soup = BeautifulSoup(response, 'lxml')

table = soup.find_all('table')[0]

single_song_url = []

row_marker = 0
for row in table.find_all('tr'):
    postfix = row.find_all('td')[1].find('a', href=True)['href']
    single_song_url.append(host + postfix)

for url in single_song_url:
    # print(url)
    r = requests.get(url)
    r_url = r.url
    # print(r_url)
    mp3_id = r_url[r_url.find("https://www.radiojavan.com/mp3s/mp3/") + 36:r_url.find("?playlist=")]
    # print("mp3 Id is : " + mp3_id)
    host_response = requests.post("https://www.radiojavan.com/mp3s/mp3_host", data={"id": mp3_id})
    host_json = host_response.text
    # print("host resp is : " + host_json)
    load = json.loads(host_json)
    # print(load)
    host_url = load['host']
    # print(host_url)
    song_url = host_url + "/media/mp3/mp3-256/" + mp3_id + ".mp3"
    print(song_url)
    file.write(song_url + "\n")
    # req.full_url = url

print("-----------")
